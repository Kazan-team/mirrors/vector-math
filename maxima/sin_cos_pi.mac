load("bitwise")$

sin_kernel(x) := if abs(x) <= 1 / 4 then sin(%pi * x) else 2$
cos_kernel(x) := if abs(x) <= 1 / 4 then cos(%pi * x) else 2$

sc(arg) := block(
    [x:arg, xi, xk, sk, ck, st, ct, s, c],
    xi:round(x*2),
    xk:x - xi / 2,
    sk:sin_kernel(xk),
    ck:cos_kernel(xk),
    st:if bit_and(xi, 1) < 1 then sk else ck,
    ct:if bit_and(xi, 1) < 1 then ck else sk,
    s:if bit_and(xi, 2) < 1 then st else -st,
    c:if bit_and(xi + 1, 2) < 1 then ct else -ct,
    [
        xk,
        s,
        c,
        sin(%pi * x) + 1/64,
        cos(%pi * x) + 1/64
    ])$

sc(0);
sc(1/4);
sc(1/2);
sc(3/4);
sc(1);
sc(-1/4);
sc(-1/2);
sc(-3/4);
sc(-1);

plot2d(
    sc(x),
    [x, -1.5, 1.5],
    [
        legend,
        "xk",
        "s",
        "c",
        "sin(%pi * x) + 1/64",
        "cos(%pi * x) + 1/64"
    ],
    [png_file, "./sin_cos_pi.png"]);
