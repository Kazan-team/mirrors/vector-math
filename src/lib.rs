#![no_std]
#![deny(unconditional_recursion)]

#[cfg(any(feature = "std", test))]
extern crate std;

pub mod algorithms;
pub mod f16;
#[cfg(feature = "ir")]
pub mod ir;
pub mod prim;
pub mod scalar;
#[cfg(feature = "stdsimd")]
pub mod stdsimd;
pub mod traits;
